//
//  UICollectionViewExtension.swift
//  Digio
//
//  Created by David Batista on 24/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

extension UICollectionView {
    // Register cell from class reference
    func register<T: UICollectionViewCell>(_: T.Type) {
        let collectionViewNib = UINib(nibName: T.NibName, bundle: nil)
        
        register(collectionViewNib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    // Dequeue cell from class reference
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier,
                                             for: indexPath as IndexPath) as? T
            else { fatalError("Could not find cell with identifier: \(T.reuseIdentifier)") }
        
        return cell
    }
}
