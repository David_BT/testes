//
//  UITableViewCellExtension.swift
//  Digio
//
//  Created by David Batista on 24/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

extension UITableViewCell: ReusableView, NibLoadableView {}
