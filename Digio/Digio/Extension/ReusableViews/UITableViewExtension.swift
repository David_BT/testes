//
//  UITableViewExtension.swift
//  Digio
//
//  Created by David Batista on 24/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

extension UITableView {
    // Register cell from class reference
    func register<T: UITableViewCell>(_: T.Type) {
        let tableViewNib = UINib(nibName: T.NibName, bundle: nil)
        
        register(tableViewNib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    // Dequeue cell from class reference
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T
            else { fatalError("Could not find cell with identifier: \(T.reuseIdentifier)") }
        
        return cell
    }
}
