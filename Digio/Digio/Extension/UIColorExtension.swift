//
//  UIColorExtension.swift
//  Digio
//
//  Created by Batista da Silva, David on 27/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }

    static var digioBlue: UIColor {
        return UIColor.rgb(red: 34, green: 38, blue: 65)
    }
}
