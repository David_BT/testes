//
//  UIVIewExtension.swift
//  Digio
//
//  Created by Batista da Silva, David on 28/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

extension UIView {
    func setDropShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 4.0
        self.clipsToBounds = false
        self.layer.masksToBounds = false
    }
}
