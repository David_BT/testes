//
//  UIViewControllerExtension.swift
//  Digio
//
//  Created by Batista da Silva, David on 27/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

var vSpinner: UIView?

extension UIViewController {
    func showSpinner(onView: UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let indicator = UIActivityIndicatorView.init(style: .large)
        indicator.startAnimating()
        indicator.center = spinnerView.center

        DispatchQueue.main.async {
            spinnerView.addSubview(indicator)
            onView.addSubview(spinnerView)
        }

        vSpinner = spinnerView
    }

    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }

    /// Display message in prompt view
    ///
    /// — Parameters:
    /// — message: Pass string of content message
    /// — title: Title to display Alert
    /// — completion: The block to execute after the presentation finishes.
    func showAlert(message: String, title: String = "", completion: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: completion)
        alertController.addAction(OKAction)

        self.present(alertController, animated: true, completion: nil)
    }
}
