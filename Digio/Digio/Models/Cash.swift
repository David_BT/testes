//
//  Cash.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

struct Cash: Codable {
    let title: String?
    let bannerURL: String?
    let cashDescription: String?

    enum CodingKeys: String, CodingKey {
        case title, bannerURL
        case cashDescription = "description"
    }
}
