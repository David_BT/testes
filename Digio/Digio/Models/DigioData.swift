//
//  DigioData.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

struct DigioData: Codable {
    let spotlight: [Spotlight]?
    let products: [Product]?
    let cash: Cash?
}
