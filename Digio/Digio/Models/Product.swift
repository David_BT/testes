//
//  Product.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

struct Product: Codable {
    let name: String?
    let imageURL: String?
    let productDescription: String?

    enum CodingKeys: String, CodingKey {
        case name, imageURL
        case productDescription = "description"
    }
}
