//
//  Spotlight.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

struct Spotlight: Codable {
    let name: String?
    let bannerURL: String?
    let spotlightDescription: String?

    enum CodingKeys: String, CodingKey {
        case name, bannerURL
        case spotlightDescription = "description"
    }
}
