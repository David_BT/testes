//
//  APIRequests.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import Alamofire

class APIRequests {
    static func getDate(_ completion: @escaping (Result<DigioData>) -> Void) {
        Alamofire.request(APIRouter.getDigio).responseData { response in
            switch response.result {
            case .success(let result):
                do {
                    let responseObject = try JSONDecoder().decode(DigioData.self, from: result)
                    
                    completion(.success(responseObject))
                } catch {
                    completion(.failure(error))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
