//
//  APIRouter.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    case getDigio
    
    // MARK: - APIURL
    private var baseURL: String {
        return "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com"
    }
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .getDigio:
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .getDigio:
            return "/sandbox/products"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .getDigio:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try self.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
                
        return urlRequest
    }
}
