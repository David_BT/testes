//
//  ViewProtocols.swift
//  Digio
//
//  Created by Batista da Silva, David on 27/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

protocol ReusableView: class {}
protocol NibLoadableView: class {}
protocol ReusableContent: class {}
