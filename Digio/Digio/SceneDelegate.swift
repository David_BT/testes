//
//  SceneDelegate.swift
//  Digio
//
//  Created by David Batista on 20/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = HomeViewController(viewModel: HomeViewModel())
        self.window = window
        window.makeKeyAndVisible()
    }
}
