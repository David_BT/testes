//
//  CashTableViewCell.swift
//  Digio
//
//  Created by David Batista on 24/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit
import SDWebImage

class CashTableViewCell: UITableViewCell {

    // MARK: Outlet
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var cashImageView: UIImageView!

    // MARK: Configure cell
    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        let firstAttributes = [NSAttributedString.Key.foregroundColor: UIColor.digioBlue]
        let secondAttributes = [NSAttributedString.Key.foregroundColor: UIColor.gray]

        let digioString = NSMutableAttributedString(string: "digio ", attributes: firstAttributes)
        let cashString = NSAttributedString(string: "Cash", attributes: secondAttributes)

        digioString.append(cashString)
        
        headerLabel.attributedText = digioString
        cashImageView.layer.cornerRadius = 8
    }
    
    func setCell(with cash: Cash?) {
        guard let cashURL = cash?.bannerURL else { return }

        cashImageView.sd_setImage(with: URL(string: cashURL),
                                  placeholderImage: #imageLiteral(resourceName: "loader_icon"),
                                  completed: nil)
    }
}
