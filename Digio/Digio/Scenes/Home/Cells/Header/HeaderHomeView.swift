//
//  HeaderHomeView.swift
//  Digio
//
//  Created by David Batista on 24/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class HeaderHomeView: UIView {

    // MARK: Outlet
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var greetingsLabel: UILabel!

    // MARK: View Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
        
    fileprivate func commonInit() {
        Bundle.main.loadNibNamed("HeaderHomeView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        logoImageView.layer.borderColor = UIColor.digioBlue.cgColor
        logoImageView.layer.borderWidth = 1
        logoImageView.layer.cornerRadius = logoImageView.bounds.height / 2
        
        greetingsLabel.text = "Olá, Anônimo"
    }

    // MARK: Setup View
    func setGreetings(from name: String) {
        greetingsLabel.text = "Olá, \(name)"
    }
}
