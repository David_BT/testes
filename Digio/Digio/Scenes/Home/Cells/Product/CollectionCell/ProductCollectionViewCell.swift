//
//  ProductCollectionViewCell.swift
//  Digio
//
//  Created by Batista da Silva, David on 28/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {

    // MARK: Outlet
    @IBOutlet weak var productImageView: UIImageView!

    // MARK: Configure cell
    override func awakeFromNib() {
        super.awakeFromNib()

        self.setDropShadow()
        self.layer.cornerRadius = 8
    }

    func setCell(with product: Product?) {
        guard let productURL = product?.imageURL else { return }

        productImageView.sd_setImage(with: URL(string: productURL),
                                       placeholderImage: #imageLiteral(resourceName: "loader_icon"),
                                       completed: nil)
    }
}
