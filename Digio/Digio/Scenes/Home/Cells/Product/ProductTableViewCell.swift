//
//  ProductTableViewCell.swift
//  Digio
//
//  Created by Batista da Silva, David on 28/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    // MARK: Outlet
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: Variables
    var products: [Product]?

    // MARK: Configure Cell
    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(ProductCollectionViewCell.self)

        headerLabel.text = "Produtos"
        headerLabel.textColor = .digioBlue
    }

    func setCell(with products: [Product]?) {
        self.products = products

        collectionView.reloadData()
    }
}

// MARK: - Collection View DataSource

extension ProductTableViewCell: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {

        return products?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as ProductCollectionViewCell
        cell.setCell(with: products?[indexPath.row])

        return cell
    }
}

// MARK: - Collection View Delegate Flow Layout

extension ProductTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}
