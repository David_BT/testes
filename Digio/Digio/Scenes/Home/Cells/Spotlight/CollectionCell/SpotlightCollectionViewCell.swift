//
//  SpotlightCollectionViewCell.swift
//  Digio
//
//  Created by Batista da Silva, David on 27/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class SpotlightCollectionViewCell: UICollectionViewCell {

    // MARK: Outlet
    @IBOutlet weak var spotlightImageView: UIImageView!

    // MARK: Configure cell
    override func awakeFromNib() {
        super.awakeFromNib()

        self.setDropShadow()
        spotlightImageView.layer.cornerRadius = 8
    }

    func setCell(with spotlight: Spotlight?) {
        guard let spotlightURL = spotlight?.bannerURL else { return }

        spotlightImageView.sd_setImage(with: URL(string: spotlightURL),
                                       placeholderImage: #imageLiteral(resourceName: "loader_icon"),
                                       completed: nil)
    }
}
