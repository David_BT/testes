//
//  SpotlightTableViewCell.swift
//  Digio
//
//  Created by David Batista on 24/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class SpotlightTableViewCell: UITableViewCell {

    // MARK: Outlet
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: Variables
    var spotlights: [Spotlight]?
    
    // MARK: Configure Cell
    override func awakeFromNib() {
        super.awakeFromNib()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SpotlightCollectionViewCell.self)
    }

    func setCell(with spotlights: [Spotlight]?) {
        self.spotlights = spotlights

        collectionView.reloadData()
    }
}

// MARK: - Collection View DataSource

extension SpotlightTableViewCell: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {

        return spotlights?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as SpotlightCollectionViewCell
        cell.setCell(with: spotlights?[indexPath.row])

        return cell
    }
}

// MARK: - Collection View Delegate Flow Layout

extension SpotlightTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width - 20
        let height = (width * 0.48) - 10

        return CGSize(width: width, height: height)
    }
}
