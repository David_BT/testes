//
//  HomeViewController.swift
//  Digio
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Variables
    var viewModel: HomeViewModelProtocol?

    // MARK: Initializer
    init(viewModel: HomeViewModelProtocol) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeUI()
        bindViewModel()
    }
        
    func makeUI() {
        // TableView
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.separatorStyle = .none

        // TableView Header
        let header = HeaderHomeView(frame: CGRect(x: 0, y: 0,
                                                  width: tableView.bounds.width, height: 74))
        tableView.tableHeaderView = header
        tableView.register(CashTableViewCell.self)
        tableView.register(SpotlightTableViewCell.self)
        tableView.register(ProductTableViewCell.self)
    }

    // MARK: View Model Methods

    func bindViewModel() {
        self.viewModel?.completion = dataDidLoad

        self.showSpinner(onView: self.view)
        viewModel?.viewDidLoad()
    }

    func dataDidLoad() {
        self.removeSpinner()
        
        if let data = viewModel?.dataError {
            showAlert(message: data.localizedDescription, title: "Erro", completion: nil)
        } else {
            tableView.reloadData()
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.dataCount() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SpotlightTableViewCell
            cell.setCell(with: viewModel?.digioData?.spotlight)

            return cell

        case 1:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as CashTableViewCell
            cell.setCell(with: viewModel?.digioData?.cash)

            return cell

        default:
            let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as ProductTableViewCell
            cell.setCell(with: viewModel?.digioData?.products)

            return cell
        }
    }
}
