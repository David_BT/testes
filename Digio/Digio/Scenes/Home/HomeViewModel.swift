//
//  HomeViewModel.swift
//  Digio
//
//  Created by Batista da Silva, David on 23/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import Foundation

protocol HomeViewModelProtocol {
    var completion: (() -> Void)? { get set }
    var dataError: Error? { get }
    var digioData: DigioData? { get }

    func viewDidLoad()
    func dataCount() -> Int?
}

class HomeViewModel: HomeViewModelProtocol {

    // MARK: Closure
    var completion: (() -> Void)?

    // MARK: Variables
    var dataError: Error?
    var digioData: DigioData?

    init() { }

    func viewDidLoad() {
        dataError = nil
        digioData = nil

        APIRequests.getDate { [weak self] (response) in
            guard let self =  self else { return }

            switch response {
            case .failure(let error):
                self.dataError = error

            case .success(let data):
                self.digioData = data

            }

            self.completion?()
        }
    }

    func dataCount() -> Int? {
        var count = 0

        if digioData?.cash != nil {
            count += 1
        }

        if let spotlight = digioData?.spotlight, !spotlight.isEmpty {
            count += 1
        }

        if let products = digioData?.products, !products.isEmpty {
            count += 1
        }

        return count
    }
}
