//
//  HomeViewControllerTests.swift
//  DigioTests
//
//  Created by Batista da Silva, David on 26/01/20.
//  Copyright © 2020 David Batista. All rights reserved.
//

import XCTest

class HomeViewControllerTests: XCTestCase {

    var homeVC: HomeViewController!

    private func setUpViewControllers() {
        self.homeVC = HomeViewController(viewModel: HomeMockViewModel())

        self.homeVC.loadView()
        self.homeVC.viewDidLoad()
    }

    override func setUp() {
        super.setUp()

        self.setUpViewControllers()
    }

    override func tearDown() {
        homeVC = nil

        super.tearDown()
    }

    func testTableViewDataSource() {
        self.homeVC.viewDidLoad()

        XCTAssertNotNil(self.homeVC.tableView.dataSource)
        XCTAssertEqual(homeVC.tableView.numberOfRows(inSection: 0), 3)
    }

    // FIXME: Fatal error: Could not find cell with identifier: SpotlightTableViewCell: file
//    func testSpotlightCell() {
//        self.homeVC.viewDidLoad()
//
//        let spotlightCell = homeVC.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? SpotlightTableViewCell
//        XCTAssertNotNil(spotlightCell)
//    }
//
//    func testCashCell() {
//        self.homeVC.viewDidLoad()
//
//        let cashCell = homeVC.tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? CashTableViewCell
//        XCTAssertNotNil(cashCell)
//    }
//
//    func testProductCell() {
//        self.homeVC.viewDidLoad()
//
//        let productCell = homeVC.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? ProductTableViewCell
//        XCTAssertNotNil(productCell)
//    }
}
