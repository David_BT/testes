//
//  HomeViewModelTests.swift
//  DigioTests
//
//  Created by Batista da Silva, David on 23/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import XCTest

class HomeViewModelTests: XCTestCase {

    var viewModel: HomeViewModelProtocol?

    override func setUp() {
        super.setUp()

        self.viewModel = HomeViewModel()
    }

    override func tearDown() {
        self.viewModel = nil
    }

    func testViewDidLoad() {
        let ex = expectation(description: "Expecting a viewModel is load")

        self.viewModel?.completion = {
            ex.fulfill()

            XCTAssertNil(self.viewModel?.dataError)
            XCTAssertNotNil(self.viewModel?.digioData)
            XCTAssertTrue(self.viewModel?.dataCount() == 3)
        }

        self.viewModel?.viewDidLoad()

        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
}
