//
//  HomeMockViewModel.swift
//  Digio
//
//  Created by Batista da Silva, David on 26/01/20..
//  Copyright © 2020 David Batista. All rights reserved.
//

import Foundation

class HomeMockViewModel: HomeViewModelProtocol {
    var completion: (() -> Void)?
    var dataError: Error?
    var digioData: DigioData?

    func viewDidLoad() {
        let product = Product(name: "Produt", imageURL: nil, productDescription: nil)
        let cash = Cash(title: "Cash", bannerURL: nil, cashDescription: nil)
        let spotlight = Spotlight(name: "Spotlight", bannerURL: nil, spotlightDescription: nil)
        self.digioData = DigioData(spotlight: [spotlight], products: [product], cash: cash)

        self.completion?()
    }

    func dataCount() -> Int? {
        return 3
    }
}
