//
//  NetworkLayerTests.swift
//  DigioTests
//
//  Created by Batista da Silva, David on 22/11/19.
//  Copyright © 2020 David Batista. All rights reserved.
//

import XCTest

class NetworkLayerTests: XCTestCase {
    
    func testGettingJSON() {
        let ex = expectation(description: "Expecting a JSON data not nil")
        
        APIRequests.getDate { (result) in
            
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let data):
                XCTAssertNotNil(data, "JSON data is nil")
            }
            ex.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
}
